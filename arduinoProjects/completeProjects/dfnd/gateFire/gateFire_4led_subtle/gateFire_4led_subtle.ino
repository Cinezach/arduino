#include <FastLED.h>

#define LED_PIN1    6 // Define the pin for the first set of LEDs
#define LED_PIN2    4 // Define the pin for the second set of LEDs
#define COLOR_ORDER GRB
#define CHIPSET     WS2811
#define NUM_LEDS    20

#define FRAMES_PER_SECOND 50

bool gReverseDirection = false;

CRGB leds1[NUM_LEDS]; // LED array for the first set
CRGB leds2[NUM_LEDS]; // LED array for the second set

CRGBPalette16 gPal1; // Palette for the first set
CRGBPalette16 gPal2; // Palette for the second set

int const potPin1 = A0; // Analog input for controlling brightness of the first set
int potVal1;
int angle1;

int const potPin2 = A2; // Analog input for controlling brightness of the second set
int potVal2;
int angle2;

#define COOLING 55
#define SPARKING 120

void setup() {
  delay(3000);
  FastLED.addLeds<CHIPSET, LED_PIN1, COLOR_ORDER>(leds1, NUM_LEDS).setCorrection(TypicalLEDStrip);
  FastLED.addLeds<CHIPSET, LED_PIN2, COLOR_ORDER>(leds2, NUM_LEDS).setCorrection(TypicalLEDStrip);
  FastLED.setBrightness(200);

  // Define your color palettes for each LED set here
  gPal1 = CRGBPalette16(CRGB::OrangeRed, CRGB::DarkRed, CRGB::OrangeRed, CRGB::Black);
  gPal2 = CRGBPalette16(CRGB::Black, CRGB::Green, CRGB::Purple, CRGB::Pink);

  Serial.begin(9600);
}

void loop() {
  // Analog input for controlling brightness of the first set
  potVal1 = analogRead(potPin1);
  angle1 = map(potVal1, 0, 1023, 0, 255);
  FastLED.setBrightness(255 - angle1);

  // Analog input for controlling brightness of the second set
  potVal2 = analogRead(potPin2);
  angle2 = map(potVal2, 0, 1023, 0, 255);
  FastLED.setBrightness(255 - angle2);

  // Call the functions for each LED set with their respective palettes
  Fire2012WithPalette(leds1, gPal1);
  Fire2012WithPalette(leds2, gPal2);

  FastLED.show();
  FastLED.delay(1000 / FRAMES_PER_SECOND);
}

void Fire2012WithPalette(CRGB *ledArray, CRGBPalette16 palette) {
  static uint8_t heat[NUM_LEDS];

  // Your Fire2012WithPalette function code here...
  // Make sure to use 'ledArray' and 'palette' instead of 'leds' and 'gPal'


  // Step 1.  Cool down every cell a little
    for( int i = 0; i < NUM_LEDS; i++) {
      heat[i] = qsub8( heat[i],  random8(0, ((COOLING * 10) / NUM_LEDS) + 2));
    }
  
    // Step 2.  Heat from each cell drifts 'up' and diffuses a little
    for( int k= NUM_LEDS - 1; k >= 2; k--) {
      heat[k] = (heat[k - 1] + heat[k - 2] + heat[k - 2] ) / 3;
    }
    
    // Step 3.  Randomly ignite new 'sparks' of heat near the bottom
    if( random8() < SPARKING ) {
      int y = random8(7);
      heat[y] = qadd8( heat[y], random8(160,255) );
    }

    // Step 4.  Map from heat cells to LED colors
    for( int j = 0; j < NUM_LEDS; j++) {
      // Scale the heat value from 0-255 down to 0-240
      // for best results with color palettes.
      uint8_t colorindex = scale8( heat[j], 240);
      CRGB color = ColorFromPalette( gPal, colorindex);
      int pixelnumber;
      if( gReverseDirection ) {
        pixelnumber = (NUM_LEDS-1) - j;
      } else {
        pixelnumber = j;
      }
      leds[pixelnumber] = color;
    }
}

