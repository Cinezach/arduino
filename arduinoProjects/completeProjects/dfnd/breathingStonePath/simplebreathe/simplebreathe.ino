#include <Adafruit_NeoPixel.h>

#define LED_PIN 6  // Change this to the appropriate pin number (Digital Pin 1)
#define NUM_LEDS 16  // Change this to match the number of LEDs in your strip

Adafruit_NeoPixel strip(NUM_LEDS, LED_PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  strip.begin();
  strip.show(); // Initialize all pixels to off
}

void loop() {
  // Define the colors for crossfade
  uint32_t color1 = strip.Color(0, 0, 255);   // Red
  uint32_t color2 = strip.Color(0, 0, 100);   // Green

  // Crossfade effect
  for (int j = 0; j <= 255; j++) {
    uint32_t color;

    if (j <= 127) {
      color = strip.gamma32(strip.Color(
        ((127 - j) * ((color1 >> 16) & 0xFF) + j * ((color2 >> 16) & 0xFF)) / 127,
        ((127 - j) * ((color1 >> 8) & 0xFF) + j * ((color2 >> 8) & 0xFF)) / 127,
        ((127 - j) * (color1 & 0xFF) + j * (color2 & 0xFF)) / 127
      ));
    }

    for (int i = 0; i < NUM_LEDS; i++) {
      strip.setPixelColor(i, color);
    }

    strip.show();
    delay(30);
  }

  // Smooth transition between effects
  for (int k = 0; k < 10; k++) {
    for (int j = 255; j >= 0; j--) {
      uint32_t color;

      if (j <= 127) {
        color = strip.gamma32(strip.Color(
          ((127 - j) * ((color2 >> 16) & 0xFF) + j * ((color1 >> 16) & 0xFF)) / 127,
          ((127 - j) * ((color2 >> 8) & 0xFF) + j * ((color1 >> 8) & 0xFF)) / 127,
          ((127 - j) * (color2 & 0xFF) + j * (color1 & 0xFF)) / 127
        ));
      } else {
        color = strip.gamma32(strip.Color(
          ((j - 128) * ((color2 >> 16) & 0xFF) + (255 - j) * ((color1 >> 16) & 0xFF)) / 127,
          ((j - 128) * ((color2 >> 8) & 0xFF) + (255 - j) * ((color1 >> 8) & 0xFF)) / 127,
          ((j - 128) * (color2 & 0xFF) + (255 - j) * (color1 & 0xFF)) / 127
        ));
      }

      for (int i = 0; i < NUM_LEDS; i++) {
        strip.setPixelColor(i, color);
      }

      strip.show();
      delay(30);
    }
  }
}
