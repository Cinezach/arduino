# Dial Control Panel Wiring and Pin out
## Materials Needed
### 
- [4x Adafruit Neo Pixel Ring](https://www.adafruit.com/product/1586?gclid=Cj0KCQjwsp6pBhCfARIsAD3GZuaStzK0yKPSTWHit69neM8kuvZ8qJ7suMGepf7f1suqPuyRfxmbdeEaAmdzEALw_wcB)
- [5x Potentiometers](https://www.mouser.com/ProductDetail/Bourns/PDB181-K425K-105B?qs=5aG0NVq1C4wGQvTe3o%2Fqwg%3D%3D&mgh=1&gclid=Cj0KCQjwsp6pBhCfARIsAD3GZuaub1Ra1aIDiR8H2ugHnxNkXRkuA7wQzfogBA-QsoEFZoF4ZKhWK8UaAgEbEALw_wcB) 

## Power Requirements
xxxx

## Pin Mapping 

#### - The Neo Pixel's DIN (data input pin) will be connected to the Arduino's digital port D6, D5, D3 and D9 respectively. 

#### - The Potentiometers (POT) will be connected to the Arduino's analog port A0, A5, A3, A4, and A1 respectively. 


###
1. RING #1 
    - DIN is connected to Arduino digital port: 6~
    - Potentiometer #1- Output terminal is connected to Arduino analog in port: A0 

2. RING #2- 
    - DIN is connected to Arduino digital port: 5~
    - Potentiometer #2- Output terminal is connected to Arduino analog in port: A5 

3. RING #3-
    - DIN is connected to Arduino digital port: 3~
    - Potentiometer #3- Output terminal is connected to Arduino analog in port: A3

4. RING #4-
    - DIN is connected to Arduino digital port: 9~
    - Potentiometer #4- Output terminal is connected to Arduino analog in port: A4

5. Dimmer
    - Potentiometer #5- Output terminal is connected to Arduino analog in port: A1


Define the number of LEDs on your ring. I was using a 12 LED ring for this project. 
You will need to adjust these numbers in each corresponding LED RING block of code if you want to use any other number of LEDs.