import org.firmata4j.IODevice;
import org.firmata4j.Pin;
import org.firmata4j.firmata.FirmataDevice;

import java.io.IOException;

public class SimpleExampleRead {

    public static void main(String[] args) {
        String myPort = "COM14"; // Modify for your own computer & setup.

        IODevice myGroveBoard = null;

        try {
            myGroveBoard = new FirmataDevice(myPort);
            myGroveBoard.start(); // Start comms with the board.
            System.out.println("Board started.");
            myGroveBoard.ensureInitializationIsDone();

            var myLED = myGroveBoard.getPin(4);
            myLED.setMode(Pin.Mode.OUTPUT);

            int numberOfBlinks = 5; // Set the number of times to blink the LED

            for (int i = 0; i < numberOfBlinks; i++) {
                // LED D4 on.
                myLED.setValue(1);

                // Pause for half a second.
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt(); // Restore interrupted status
                    System.out.println("Sleep interrupted.");
                }

                // LED D4 off.
                myLED.setValue(0);

                // Pause for half a second.
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt(); // Restore interrupted status
                    System.out.println("Sleep interrupted.");
                }
            }

        } catch (IOException ex) {
            System.out.println("An IOException occurred: " + ex.getMessage());
        } catch (Exception ex) {
            System.out.println("An error occurred: " + ex.getMessage());
        } finally {
            if (myGroveBoard != null) {
                try {
                    myGroveBoard.stop(); // Finish with the board.
                    System.out.println("Board stopped.");
                } catch (IOException ex) {
                    System.out.println("An IOException occurred while stopping: " + ex.getMessage());
                }
            }
        }
    }
}


//Adding more fluff
