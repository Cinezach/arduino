

[![Controller Images](https://gitlab.com/Cinezach/arduino/-/raw/main/resources/images/projectImages/multiPotController/multiPot_button_arduino.jpg "View all controller images")](https://gitlab.com/Cinezach/arduino/-/tree/main/resources/images/projectImages/multiPotController)

# Hardware used in this project
1. Elegoo Arduino Mega 2560 R3
2. Potentiometer
3. Momentary Switch
4. Prototyping circuit board 
5. Header Pins
6. X gauge wiring

detail sections to be added: Sources ,Hardware, wiring, build, 