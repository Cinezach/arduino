#include <Adafruit_NeoPixel.h>



//Connect the ring's 'data in' to digital pin 6, 5, 3 and 9 respectively on the arduino. 

//Pin Mapping//>>
//RING #1- data pin is connected to Arduino digital port: 6~
//Potentiometer #1- data pin is connected to Arduino analog in port: A0 

//RING #2- data pin is connected to Arduino digital port: 5~
//Potentiometer #2- data pin is connected to Arduino analog in port: A5 

//RING #3- data pin is connected to Arduino digital port: 3~
//Potentiometer #3- data pin is connected to Arduino analog in port: A3

//RING #4- data pin is connected to Arduino digital port: 9~
//Potentiometer #4- data pin is connected to Arduino analog in port: A4

//Dimmer//
//Potentiometer #5- data pin is connected to Arduino analog in port: A1

//Define the number of LEDs on your ring. I was using a 12 LED ring for this project. 
//You will need to adjust these numbers in each corresponding LED RING block of code if you want to use any other number of LEDs.
#define PIN 6 
Adafruit_NeoPixel ring = Adafruit_NeoPixel(12, PIN, NEO_GRB + NEO_KHZ800);

#define PIN 5
Adafruit_NeoPixel ring2 = Adafruit_NeoPixel(12, PIN, NEO_GRB + NEO_KHZ800);

#define PIN 3
Adafruit_NeoPixel ring3 = Adafruit_NeoPixel(12, PIN, NEO_GRB + NEO_KHZ800);

#define PIN 9
Adafruit_NeoPixel ring4 = Adafruit_NeoPixel(12, PIN, NEO_GRB + NEO_KHZ800);

//This contains the potentiometer values conversion.
int numOn; 
int numOn2;
int numOn3;
int numOn4;
int lux;


void setup() {

  ///RING1 SETUP///
  ring.begin();
  ring.show(); 
  numOn = 0;
  pinMode(A0, INPUT);

  ///RING2 SETUP///
  ring2.begin();
  ring2.show(); 
  numOn2 = 0;
  pinMode(A5, INPUT);  

  ///RING3 SETUP/// 
  ring3.begin();
  ring3.show(); 
  numOn3 = 0;  
  pinMode(A3, INPUT);

  ///RING4 SETUP/// 
  ring4.begin();
  ring4.show(); 
  numOn4 = 0;  
  pinMode(A4, INPUT);

  ///BRIGHTNESS///
  lux = 0;
  pinMode(A1, INPUT);

//Serial.begin(57600);
}

void loop() {
//Uncomment the next four lines to test potentiometer values on serial monitor.
//Serial.println("numONconversion");
//Serial.println(numOn);
//Serial.println("reading");
//Serial.println(analogRead(A0));



///RING 1///
 int reading = analogRead(A0);
numOn = (int)(reading * 14 / 1022); //potentiometer specific values, adjust first number x/1022 until entire ring is filled. MUST RESULT IN WHOLE INT.

for (int i = 0; i < 12; i++) {
  if (i < numOn) {
    if (i < 4) {
      ring.setPixelColor(i, 0, 255, 0); // green for first 4 LEDs
    } else if (i < 8) {
      ring.setPixelColor(i, 255, 255, 0); // yellow for next 4 LEDs
    } else {
      ring.setPixelColor(i, 255, 0, 0); // red for last 4 LEDs
    }
  } else {
    ring.setPixelColor(i, 0, 0, 0); // turn off LED
  }
}

ring.show();


///Ring2///
int reading2 = analogRead(A5);
numOn2 = (int)(reading2 * 14 / 1022);//potentiometer specific values, adjust first number x/1022 until entire ring is filled. MUST RESULT IN WHOLE INT.

for (int i = 0; i < 12; i++) {
  if (i < numOn2) {
    if (i < 4) {
      ring2.setPixelColor(i, 0, 255, 0); // green for first 4 LEDs
    } else if (i < 8) {
      ring2.setPixelColor(i, 255, 255, 0); // yellow for next 4 LEDs
    } else {
      ring2.setPixelColor(i, 255, 0, 0); // red for last 4 LEDs
    }
  } else {
    ring2.setPixelColor(i, 0, 0, 0); // turn off LED
  }
}

ring2.show();

///Ring3///
int reading3 = analogRead(A3);
numOn3 = (int)(reading3 * 14 / 1022);//potentiometer specific values, adjust first number x/1022 until entire ring is filled. MUST RESULT IN WHOLE INT.

for (int i = 0; i < 12; i++) {
  if (i < numOn3) {
    if (i < 4) {
      ring3.setPixelColor(i, 0, 255, 0); // green for first 4 LEDs
    } else if (i < 8) {
      ring3.setPixelColor(i, 255, 255, 0); // yellow for next 4 LEDs
    } else {
      ring3.setPixelColor(i, 255, 0, 0); // red for last 4 LEDs
    }
  } else {
    ring3.setPixelColor(i, 0, 0, 0); // turn off LED
  }
}

ring3.show();


///Ring4///
// LED Data 
int reading4 = analogRead(A4);
numOn4 = (int)(reading4 * 14 / 1022);//potentiometer specific values, adjust first number x/1022 until entire ring is filled. MUST RESULT IN WHOLE INT.

for (int i = 0; i < 12; i++) {
  if (i < numOn4) {
    if (i < 4) {
      ring4.setPixelColor(i, 0, 255, 0); // green for first 4 LEDs
    } else if (i < 8) {
      ring4.setPixelColor(i, 255, 255, 0); // yellow for next 4 LEDs
    } else {
      ring4.setPixelColor(i, 255, 0, 0); // red for last 4 LEDs
    }
  } else {
    ring4.setPixelColor(i, 0, 0, 0); // turn off LED
  }
}

ring4.show();

//GLOBAL BRIGHTNESS//
//Data into port A1
int reading5 = analogRead(A1);
lux = (int)(reading5 * 14 / 1022);//Adjust number x/1022 for global dimming threshold. Don't let the smoke out.
ring.setBrightness(lux); 
ring2.setBrightness(lux); 
ring3.setBrightness(lux); 
ring4.setBrightness(lux); 

}



