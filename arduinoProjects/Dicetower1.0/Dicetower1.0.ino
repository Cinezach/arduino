
// these constants won't change:
const int ledPin = 13;       // LED connected to digital pin 13
// variable to hold sensor value
int sensorValue;
// variable to calibrate low value
int sensorLow = 1023;
// variable to calibrate high value
int sensorHigh = 0;





// A basic everyday NeoPixel ring test program.

// NEOPIXEL BEST PRACTICES for most reliable operation:
// - Add 1000 uF CAPACITOR between NeoPixel ring's + and - connections.
// - MINIMIZE WIRING LENGTH between microcontroller board and first pixel.
// - NeoPixel ring's DATA-IN should pass through a 300-500 OHM RESISTOR.
// - AVOID connecting NeoPixels on a LIVE CIRCUIT. If you must, ALWAYS
//   connect GROUND (-) first, then +, then data.
// - When using a 3.3V microcontroller with a 5V-powered NeoPixel ring,
//   a LOGIC-LEVEL CONVERTER on the data line is STRONGLY RECOMMENDED.
// (Skipping these may work OK on your workbench but can fail in the field)

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1:
#define LED_PIN    6
#define LED_COUNT 50

byte ledNumOfRings  = 2;                        // number of rings
int ledPos[][2] = {                             // positions of the leds for each ring. note: start with 0
  { 0, 24},
  {25, 50},
};

Adafruit_NeoPixel ring(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

// How many NeoPixels are attached to the Arduino?


// Declare our NeoPixel ring object:

// Argument 1 = Number of pixels in NeoPixel ring
// Argument 2 = Arduino pin number (most are valid)
// Argument 3 = Pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)


// setup() function -- runs once at startup --------------------------------

 

void setup() {
//>>>>>SENSOR SETUP<<<<<
  // Make the LED pin an output and turn it on
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);

  // calibrate for the first five seconds after program runs
  while (millis() < 5000) {
    // record the maximum sensor value
    sensorValue = analogRead(A0);
    if (sensorValue > sensorHigh) {
      sensorHigh = sensorValue;
    }
    // record the minimum sensor value
    if (sensorValue < sensorLow) {
      sensorLow = sensorValue;
    }
  }
  // turn the LED off, signaling the end of the calibration period
  digitalWrite(ledPin, LOW);
//>>>>>>>LED STRIP SETUP<<<<<<<<<<
 // Serial.begin(9600);       // use the serial port
  ring.begin();           // INITIALIZE NeoPixel ring object (REQUIRED)
  ring.show();            // Turn OFF all pixels ASAP
  ring.setBrightness(200); // Set BRIGHTNESS to about 1/5 (max = 255)
}

void loop() {
  
 //>>>>SENSOR GO<<<<<<<

  //read the input from A0 and store it in a variable
  sensorValue = analogRead(A0);
  // map the sensor values to a wide range of pitches
  int diceRoll = map(sensorValue, sensorLow, sensorHigh, 10, 1023);
  // if the sensor reading is greater than the threshold:

if (diceRoll < 50){
 int firstPixelHue = 0;     // First pixel starts at red (hue 0)
  for(int a=0; a<1; a++) {  // Repeat x times...
    for(int b=0; b<1; b++) { //  'b' counts from 0 to x loops?...
      ring.clear();         //   Set all pixels in RAM to 0 (off)
      // 'c' counts up from 'b' to end of ring in increments of ...
      for(int c=b; c<ring.numPixels(); c += 1) {
        // hue of pixel 'c' is offset by an amount to make one full
        // revolution of the color wheel (range 65536) along the length
        // of the ring (ring.numPixels() steps):
        int      hue   = firstPixelHue + c * 65536L / ring.numPixels();
        uint32_t color = ring.gamma32(ring.ColorHSV(hue)); // hue -> RGB
        ring.setPixelColor(c, color); // Set pixel 'c' to value 'color'
      
      ring.show();                // Update ring with new contents
      delay(100);                 // Pause for a moment
      firstPixelHue += 65536 / 90; // One cycle of color wheel over 90 frames
    ring.clear();
    }
 //ring.clear(); //uncomment this to make rainbow stay on
 } 
  }
}
}  
  

 