# Arduino Projects
## LED Control

|Project|Boards|Related Projects|About|Media|Sources|
|----|--|-----|-----|--|--|
|[Dragon Fire Simulation](https://gitlab.com/Cinezach/arduino/-/tree/main/arduinoProjects/completeProjects/dfnd/fireSimulation?ref_type=heads) ![Dragon Fire Image](https://gitlab.com/Cinezach/arduino/-/raw/main/resources/images/projectImages/dragonFire/393321168_1068393754340279_1858051710200663070_n.jpg?ref_type=heads)|Arduino Nano|[Fire Simulation Controller](https://gitlab.com/Cinezach/arduino/-/blob/main/arduinoProjects/completeProjects/hardware/README.md?ref_type=heads)[![Controller Images](https://gitlab.com/Cinezach/arduino/-/raw/main/resources/images/projectImages/multiPotController/multiPot_button_arduino.jpg "View all controller images")](https://gitlab.com/Cinezach/arduino/-/tree/main/resources/images/projectImages/multiPotController)| Simulate the characteristics of a bellowing fire by projecting light through a column of vaporized dry ice.| [Clip 1](https://youtube.com/clip/UgkxhUlyXbiQVMi1Dct7DqeeuJFQzw_DOvf7?si=HhNx-XpIpsjGuFKI) [Clip 2](https://youtube.com/clip/Ugkxz1FAW_59R6Huuf6Ird4_14_4utayNPQm?si=pcNwpRdabvQFfbzw) |Derived from FASTLED Library: Fire2012WithPalette.ino|
|[Park Gates](https://gitlab.com/Cinezach/arduino/-/tree/main/arduinoProjects/completeProjects/dfnd/gateFire?ref_type=heads)![Park Gates](https://gitlab.com/Cinezach/arduino/-/raw/main/resources/images/projectImages/gate/20230617_161156.jpg?ref_type=heads "View all Gate Images")|Arduino Nano| While this project is meant to be self contained the hardware is compatible with the Fire Simulation Controller.  | Torch fire simulation on park gates. |[Clip](https://gitlab.com/Cinezach/arduino/-/raw/main/resources/images/projectImages/gate/20230615_203529.mp4?ref_type=heads)| - Derived from FASTLED  Library: Fire2012WithPalette.ino <br />-Gate construction: Rebekah Estey|
|[Stone Path](https://gitlab.com/Cinezach/arduino/-/tree/main/arduinoProjects/completeProjects/dfnd/breathingStonePath?ref_type=heads)![Stone Path](https://gitlab.com/Cinezach/arduino/-/raw/main/resources/images/projectImages/gate/20230617_161156.jpg?ref_type=heads "View all Path Images")|Arduino Nano| Basic LED animation for a glowing path. | Torch fire simulation on park gates. |[Clip](https://gitlab.com/Cinezach/arduino/-/raw/main/resources/images/projectImages/stonePath/20230522_201505.mp4?ref_type=heads)| - Derived from FASTLED  Library: Fire2012WithPalette.ino <br />-Path construction: Rebekah Hunter, Tawny Tran|



## Category 2

|x|Column2|Column3|
|--|--|--|
|Row1Column1dd2p|Row1Column2|Row1Column3|
|Row2Column1|Row2Column2|Row2Column3|
|Row3Column1|Row3Column2|Row3Column3|