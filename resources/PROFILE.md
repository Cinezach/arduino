
## Senior Cinema Camera Technician
### Base Responsibilities
- Oversee quality assurance and maintenance to ensure
uniformity in standards of practice among technicians in
North American and European facilities.

- Perform high-level mechanical and electronic maintenance upon
premium, industry-standard equipment to meet company and
manufacturer specifications.

### Outreach
- Foster relationships with leading companies in the motion picture
industry, and provide technical assistance to clients on inquiries.
- Spearhead the training and development of global technical and
operational staff by setting a high standard through example.
- Identify and cultivate alternative solutions to overcome
    design challenges, and share feedback to manufacturers
    regarding product improvements

### Custom Fabrication

- Design and oversee the manufacture of mechanical
solutions for proprietary camera support systems, enabling
dynamic compatibility between new and legacy devices. [^1] 


   

[^1]: [My DBA-1 as featured in *American Cinematographer's* **"New Products"** article series.](https://theasc.com/articles/dynamic-releases-dba-1)




