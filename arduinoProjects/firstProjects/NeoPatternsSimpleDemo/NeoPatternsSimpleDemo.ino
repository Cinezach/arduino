
 

#include <Adafruit_NeoPixel.h>

#define PIN 6
#define NUMPIXEL 24  //how many pixel on the strip.

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMPIXEL, PIN, NEO_GRB + NEO_KHZ800);

int MinBrightness = 20;       //value 0-255
int MaxBrightness = 100;      //value 0-255

int numLoops1 = 1;
int numLoops2 = 1;
//int numLoops3 = 5;
//int numLoops4 = 3;          //add new integer and value for more color's loop if needed.

int fadeInWait = 15;          //lighting up speed, steps.
int fadeOutWait = 10;         //dimming speed, steps.

//---------------------------------------------------------------------------------------------------//

void setup() {
  strip.begin();
  strip.show();
}

/*
rgbBreathe(strip.Color(insert r,g,b color code),numLoops(refer to integer above), (duration for lights to hold before dimming. insert 0 to skip hold)
rainbowBreathe(numLoops(refer to integer above),(duration for lights to hold before dimming. insert 0 to skip hold)
*/

void loop() {
  
  //---strip will stay lit for some time before dimming again.----
  
  rgbBreathe(strip.Color(0, 150, 10), numLoops1, 0); //red.
 //rainbowBreathe(numLoops1, 200);
  rgbBreathe(strip.Color(0, 50, 100), numLoops1, 0); //red.
  //rainbowBreathe(numLoops1, 200);


  //duplicate for more colors.
}


//Functions -----------------------------------------------------------------------------------------//


void rgbBreathe(uint32_t c, uint8_t x, uint8_t y) {
  for (int j = 0; j < x; j++) {
    for (uint8_t b = MinBrightness; b < MaxBrightness; b++) {
      strip.setBrightness(b * 255 / 255);
      for (uint16_t i = 0; i < strip.numPixels(); i++) {
        strip.setPixelColor(i, c);
      }
      strip.show();
      delay(fadeInWait);
    }
    strip.setBrightness(MaxBrightness * 255 / 255);
    for (uint16_t i = 0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, c);
      strip.show();
      delay(y);
    }
    for (uint8_t b = MaxBrightness; b > MinBrightness; b--) {
      strip.setBrightness(b * 255 / 255);
      for (uint16_t i = 0; i < strip.numPixels(); i++) {
        strip.setPixelColor(i, c);
      }
      strip.show();
      delay(fadeOutWait);
    }
  }
}

